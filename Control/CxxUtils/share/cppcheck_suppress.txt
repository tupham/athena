# athena-specific cppcheck suppression file

# Do not check dictionaries.
*:*Dict.cxx

# We generally ignore externals, but in a LCG "dev" nightly, LCG_RELEASE_BASE points to
# other directories using symlinks. So we ignore the LCG areas explicitly:
*:/cvmfs/sft-nightlies.cern.ch/lcg/*
*:/cvmfs/sft.cern.ch/lcg/*

# It's common to use assert with side-effects in unit tests. Don't warn about those.
assignmentInAssert:*_test.*
assertWithSideEffect:*_test.*

# Do not warn about same-named functions in class hierarchies.
duplInheritedMember

# Do not warn about syntax errors (if it compiles it is likely a bug in cppcheck).
syntaxError

# Disable warning about too complex files being ignored in check.
normalCheckLevelMaxBranches

# The recommended use of readdir_r is actually deprecated.
readdirCalled
