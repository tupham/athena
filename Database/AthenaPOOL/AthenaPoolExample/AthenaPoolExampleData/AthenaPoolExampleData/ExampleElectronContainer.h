/*
 * Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
 */

#ifndef ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONCONTAINER_H
#define ATHENAPOOLEXAMPLEDATA_EXAMPLEELECTRONCONTAINER_H

#include "AthenaPoolExampleData/versions/ExampleElectronContainer_v1.h"

namespace xAOD {
typedef ExampleElectronContainer_v1 ExampleElectronContainer;
}

// Class definition
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF(xAOD::ExampleElectronContainer, 1340937587, 1)

#endif
